package com.portal.dto;

import com.portal.model.NewGame;
import com.portal.model.Orthography;

public class NewGameDto {

    private String nameGame;

    public NewGameDto() {
    }

    public NewGameDto(NewGame newGame){
        this.nameGame = newGame.getNameGame();
    }

    public String getNameGame() {
        return nameGame;
    }

    public void setNameGame(String nameGame) {
        this.nameGame = nameGame;
    }


}
