package com.portal.dto;

import com.portal.model.Orthography;

public class OrthographyDto {

    private Long id;

    private String word;

    private String wordAnglish;

    private String wrongLetter;

    private String pathImage;

    private String pathSound;

    public OrthographyDto() {
    }

    public OrthographyDto(Orthography orthography){
        this.id = orthography.getId();
        this.word = orthography.getWord();
        this.wordAnglish = orthography.getWordAnglish();
        this.wrongLetter = orthography.getWrongLetter();
        this.pathImage = orthography.getPathImage();
        this.pathSound = orthography.getPathSound();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWordAnglish() {
        return wordAnglish;
    }

    public void setWordAnglish(String wordAnglish) {
        this.wordAnglish = wordAnglish;
    }

    public String getWrongLetter() {
        return wrongLetter;
    }

    public void setWrongLetter(String wrongLetter) {
        this.wrongLetter = wrongLetter;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public String getPathSound() {
        return pathSound;
    }

    public void setPathSound(String pathSound) {
        this.pathSound = pathSound;
    }
}
