package com.portal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultGamesDto {

    private String name;

    private Integer point;

    private Integer good;

    private Integer wrong;

    private String nameGame;

    @JsonProperty
    private Boolean isAright;

    public ResultGamesDto() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getWrong() {
        return wrong;
    }

    public void setWrong(Integer wrong) {
        this.wrong = wrong;
    }

    public String getNameGame() {
        return nameGame;
    }

    public void setNameGame(String nameGame) {
        this.nameGame = nameGame;
    }

    public Boolean getAright() {
        return isAright;
    }

    public void setAright(Boolean aright) {
        isAright = aright;
    }
}
