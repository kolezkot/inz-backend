package com.portal.dto;

import com.portal.model.User;

public class UserDto {

    private String username;

    private String password;

    private String email;

    private Integer age;

    private String surname;

    private String name;

    public UserDto(){

    }

    public UserDto(User user) {
        this.name = user.getName();
        this.username = user.getUsername();
        this.surname = user.getSurname();
        this.age = user.getAge();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
