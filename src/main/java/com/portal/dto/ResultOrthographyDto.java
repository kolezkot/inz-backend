package com.portal.dto;

import com.portal.model.ResultOrthography;

public class ResultOrthographyDto {

    private Long idChild;

    private Long idOrthography;

    private Integer good;

    private Integer wrong;

    public ResultOrthographyDto() {
    }

    public ResultOrthographyDto (ResultOrthography resultOrthography) {
        this.idChild = resultOrthography.getUser().getId();
        this.idOrthography = resultOrthography.getOrthography().getId();
        this.good = resultOrthography.getGood();
        this.wrong = resultOrthography.getWrong();
    }

    public Long getIdChild() {
        return idChild;
    }

    public void setIdChild(Long idChild) {
        this.idChild = idChild;
    }

    public Long getIdOrthography() {
        return idOrthography;
    }

    public void setIdOrthography(Long idOrthography) {
        this.idOrthography = idOrthography;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getWrong() {
        return wrong;
    }

    public void setWrong(Integer wrong) {
        this.wrong = wrong;
    }
}
