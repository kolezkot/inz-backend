package com.portal.dto;

import com.portal.model.ResultDigits;

public class ResultDigitsDto {

    private Long idChild;

    private Integer digit;

    private Integer good;

    private Integer wrong;

    public ResultDigitsDto() {
    }

    public ResultDigitsDto (ResultDigits result) {
        this.idChild = result.getUser().getId();
        this.digit = result.getDigit();
        this.good = result.getGood();
        this.wrong = result.getWrong();
    }

    public Long getIdChild() {
        return idChild;
    }

    public void setIdChild(Long idChild) {
        this.idChild = idChild;
    }

    public Integer getDigit() {
        return digit;
    }

    public void setDigit(Integer digit) {
        this.digit = digit;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getWrong() {
        return wrong;
    }

    public void setWrong(Integer wrong) {
        this.wrong = wrong;
    }
}
