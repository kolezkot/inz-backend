package com.portal.dto;

import com.portal.model.ResultWords;
import com.portal.model.Word;

public class ResultWordDto {

    private Long idChild;

    private Long idWord;

    private Integer good;

    private Integer wrong;

    public ResultWordDto() {
    }

    public ResultWordDto (ResultWords result) {
        this.idChild = result.getUser().getId();
        this.idWord = result.getWord().getId();
        this.good = result.getGood();
        this.wrong = result.getWrong();
    }

    public Long getIdChild() {
        return idChild;
    }

    public void setIdChild(Long idChild) {
        this.idChild = idChild;
    }

    public Long getIdWord() {
        return idWord;
    }

    public void setIdWord(Long idWord) {
        this.idWord = idWord;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getWrong() {
        return wrong;
    }

    public void setWrong(Integer wrong) {
        this.wrong = wrong;
    }
}
