package com.portal.dto;

import com.portal.model.Word;

public class WordDto {

    private Long id;

    private String word;

    private String pathWord;

    private String englishWord;

    public WordDto() {
    }

    public WordDto (Word word) {

        this.id = word.getId();
        this.word = word.getWord();
        this.pathWord = word.getPathWord();
        this.englishWord = word.getEnglishWord();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPathWord() {
        return pathWord;
    }

    public void setPathWord(String pathWord) {
        this.pathWord = pathWord;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }
}
