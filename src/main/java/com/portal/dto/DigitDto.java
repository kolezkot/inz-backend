package com.portal.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DigitDto {

    private Integer digit;

    @JsonProperty
    private Boolean isGood;

    public DigitDto() {
    }

    public Integer getDigit() {
        return digit;
    }

    public void setDigit(Integer digit) {
        this.digit = digit;
    }

    public Boolean getGood() {
        return isGood;
    }

    public void setGood(Boolean good) {
        isGood = good;
    }
}
