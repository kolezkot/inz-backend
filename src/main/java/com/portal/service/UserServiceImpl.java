package com.portal.service;

import com.portal.config.jwt.AuthenticationService;
import com.portal.dto.UserDto;
import com.portal.exception.ValidationException;
import com.portal.model.User;
import com.portal.model.UserRole;
import com.portal.repository.UserRepository;
import com.portal.repository.WordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public void register(UserDto userDto) throws ValidationException {
        if (userRepository.findOneByUsername(userDto.getUsername()) != null) {
            throw new ValidationException("Username already exist");
        }
        User user = new User(userDto);
        user.setRole(UserRole.PARENT);
        userRepository.save(user);
    }

    @Override
    public void registerChild(UserDto userDto) throws ValidationException {
        if (userRepository.findOneByUsername(userDto.getUsername()) != null) {
            throw new ValidationException("Username already exist");
        }
        User parentUser = authenticationService.getCurrentlyLoggedUser();
        if (parentUser == null) {
            throw new ValidationException("Parent cannot be null");
        }
        User user = new User(userDto);
        user.setParentUser(parentUser);
        user.setRole(UserRole.CHILD);
        user.setSurname(parentUser.getSurname());
        userRepository.save(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream().map(UserDto::new).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> getAllChildUsers(UserDto parentUser) {
        User user = userRepository.findOneByUsername(parentUser.getUsername());
        return userRepository.findAllByParentUser(user).stream().map(UserDto::new).collect(Collectors.toList());
    }


    @Override
    public void updateUser(UserDto userDto) throws ValidationException {
        User user = userRepository.findOneByUsername(userDto.getUsername());
        if (user == null) {
            throw new ValidationException("Username doesn't exist");
        }
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        userRepository.save(user);
    }

    @Override
    public UserDto findUserById(String username) throws ValidationException {
        User user = userRepository.findOneByUsername(username);
        if (user != null) {
            return new UserDto(user);
        } else throw new ValidationException("User not found");
    }


}
