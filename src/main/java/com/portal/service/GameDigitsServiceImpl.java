package com.portal.service;

import com.portal.config.jwt.AuthenticationService;
import com.portal.dto.DigitDto;
import com.portal.dto.ResultDigitsDto;
import com.portal.exception.ValidationException;
import com.portal.model.ResultDigits;
import com.portal.model.User;
import com.portal.repository.ResultDigitsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GameDigitsServiceImpl implements GameDigitsService {

    @Autowired
    private ResultDigitsRepository resultDigitsRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public void saveResultDigit(DigitDto digitDto) throws ValidationException {
        User user = authenticationService.getCurrentlyLoggedUser();
        if (user == null) {
            throw new ValidationException("Child cannot be null");
        }
        //Integer digit = resultDigitsRepository.findOne(resultDigitDto.getDigit());

        ResultDigits resultDigit = null;
        Optional<ResultDigits> resultDigitsOptional = resultDigitsRepository.findOneByUserAndDigit(user, digitDto.getDigit());
        if (digitDto.getGood()) {
            if (resultDigitsOptional.isPresent()) {
                resultDigit = resultDigitsOptional.get();
                resultDigit.increaseGood();
            } else {
                resultDigit = new ResultDigits();
                resultDigit.setDigit(digitDto.getDigit());
                resultDigit.setGood(1);
                resultDigit.setUser(user);
            }
        } else {
            if (resultDigitsOptional.isPresent()) {
                resultDigit = resultDigitsOptional.get();
                resultDigit.increaseWrong();
            } else {
                resultDigit = new ResultDigits();
                resultDigit.setDigit(digitDto.getDigit());
                resultDigit.setWrong(1);
                resultDigit.setUser(user);
            }
        }
        resultDigitsRepository.save(resultDigit);
    }
}
