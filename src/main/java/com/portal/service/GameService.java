package com.portal.service;

import com.portal.dto.*;
import com.portal.exception.ValidationException;
import com.portal.model.ResultGames;
import com.portal.model.ResultOrthography;
import com.portal.model.User;

import java.util.List;

public interface GameService {

    void saveResultWord(ResultWordDto result) throws ValidationException;

    List<ResultGamesDto> findResultWordByUsername(String username) throws ValidationException;

    List<ResultGamesDto> findResultDigitByUsername(String username) throws ValidationException;

    List<WordDto> getAllWords();

    List<OrthographyDto> getAllOrthography();

    void saveResultOrthography(ResultOrthographyDto resultOrthographyDto) throws ValidationException;

    List<ResultGamesDto> findResultOrthographyByUsername(String username) throws ValidationException;

    void saveResultGame(ResultGamesDto resultGamesDto) throws ValidationException;

    List<ResultGamesDto>findResultGamesByUsernameAndNameGame(String username, String nameGame) throws ValidationException;

    void saveNewNameGame(NewGameDto newGameDto) throws ValidationException;

}
