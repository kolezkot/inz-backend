package com.portal.service;

import com.portal.dto.DigitDto;
import com.portal.exception.ValidationException;
import com.portal.model.ResultDigits;

import java.util.List;

public interface GameDigitsService {

    void saveResultDigit (DigitDto digitDto) throws ValidationException;
}
