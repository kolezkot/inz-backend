package com.portal.service;

import com.portal.dto.UserDto;
import com.portal.exception.ValidationException;

import java.util.List;

public interface UserService {

    void register(UserDto userDto) throws ValidationException;

    List<UserDto> getAllUsers();

    List<UserDto> getAllChildUsers(UserDto parentUser);

    void updateUser(UserDto userDto) throws ValidationException;

    UserDto findUserById(String username) throws ValidationException;

    void registerChild(UserDto userDto) throws ValidationException;
    //List<ResultWordDto> getAllResult(); //!!!!!!!!!!!!!!!!!!!!!
}
