package com.portal.service;

import com.portal.config.jwt.AuthenticationService;
import com.portal.dto.*;
import com.portal.exception.ValidationException;
import com.portal.model.*;
import com.portal.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {


    @Autowired
    private ResultWordRepository resultWordRepository;

    @Autowired
    private ResultDigitsRepository resultDigitsRepository;

    @Autowired
    private ResultOrthographyRepository resultOrthographyRepository;

    @Autowired
    private ResultGamesRepository resultGamesRepository;

    @Autowired
    private NewGameRepository newGameRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WordsRepository wordsRepository;

    @Autowired
    private OrthographyRepository orthographyRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    // public List<ResultWordDto> getAllResult() {
    //    return userRepository.findAll().stream().map(ResultWordDto::new).collect(Collectors.toList());
    // }

    @Override
    public void saveResultWord(ResultWordDto resultDto) throws ValidationException {
        User user = authenticationService.getCurrentlyLoggedUser();
        if (user == null) {
            throw new ValidationException("Child cannot be null");
        }
        Word word = wordsRepository.findOne(resultDto.getIdWord());

        ResultWords resultWords = null;
        Optional<ResultWords> resultWordsOptional = resultWordRepository.findOneByUserAndWord(user, word);
        if (resultWordsOptional.isPresent()) {
            resultWords = resultWordsOptional.get();
            if (resultDto.getGood() == 1) {
                resultWords.increaseGood();
            } else if (resultDto.getWrong() == 1) {
                resultWords.increaseWrong();
            }
        } else {
            resultWords = new ResultWords();
            resultWords.setUser(user);
            resultWords.setWord(word);
            if (resultDto.getGood() == 1) {
                resultWords.setGood(1);
            } else if (resultDto.getWrong() == 1) {
                resultWords.setWrong(1);
            }
        }
        resultWordRepository.save(resultWords);
    }

    @Override
    public void saveResultOrthography(ResultOrthographyDto resultOrthographyDto) throws ValidationException {
        User user = authenticationService.getCurrentlyLoggedUser();
        if (user == null) {
            throw new ValidationException("Child cannot be null");
        }
        Orthography orthography = orthographyRepository.findOne(resultOrthographyDto.getIdOrthography());

        ResultOrthography resultOrthography = null;
        Optional<ResultOrthography> resultOrthographyOptional = resultOrthographyRepository.findOneByUserAndOrthography(user, orthography);
        if (resultOrthographyOptional.isPresent()) {
            resultOrthography = resultOrthographyOptional.get();
            if (resultOrthographyDto.getGood() == 1) {
                resultOrthography.increaseGood();
            } else if (resultOrthographyDto.getWrong() == 1) {
                resultOrthography.increaseWrong();
            }
        } else {
            resultOrthography = new ResultOrthography();
            resultOrthography.setUser(user);
            resultOrthography.setOrthography(orthography);
            if (resultOrthographyDto.getGood() == 1) {
                resultOrthography.setGood(1);
            } else if (resultOrthographyDto.getWrong() == 1) {
                resultOrthography.setWrong(1);
            }
        }
        resultOrthographyRepository.save(resultOrthography);
    }


    @Override
    public List<ResultGamesDto> findResultWordByUsername(String username) throws ValidationException {
        User user = userRepository.findOneByUsername(username);
        List<ResultWords> resultWordsList = resultWordRepository.findAllByUser(user);
        List<ResultGamesDto> resultGamesDtoList = new ArrayList<>();
        for (ResultWords resultWords : resultWordsList) {
            ResultGamesDto resultGamesDto = new ResultGamesDto();
            if (user.getAge() < 5) {
                resultGamesDto.setName(resultWords.getWord().getWord());
            } else {
                resultGamesDto.setName(resultWords.getWord().getEnglishWord());
            }
            resultGamesDto.setGood(resultWords.getGood());
            resultGamesDto.setWrong(resultWords.getWrong());
            resultGamesDtoList.add(resultGamesDto);
        }
        return resultGamesDtoList;
    }

    @Override
    public List<ResultGamesDto> findResultDigitByUsername(String username) throws ValidationException {
        User user = userRepository.findOneByUsername(username);
        List<ResultDigits> resultDigitsList = resultDigitsRepository.findAllByUser(user);
        List<ResultGamesDto> resultGamesDtoList = new ArrayList<>();
        for (ResultDigits resultDigits : resultDigitsList) {
            ResultGamesDto resultGamesDto = new ResultGamesDto();
            resultGamesDto.setPoint(resultDigits.getDigit());
            resultGamesDto.setGood(resultDigits.getGood());
            resultGamesDto.setWrong(resultDigits.getWrong());
            resultGamesDtoList.add(resultGamesDto);
        }
        return resultGamesDtoList;
    }

    @Override
    public List<WordDto> getAllWords() {
        return wordsRepository.findAll().stream().map(WordDto::new).collect(Collectors.toList());
    }

    @Override
    public List<OrthographyDto> getAllOrthography() {
        return orthographyRepository.findAll().stream().map(OrthographyDto::new).collect(Collectors.toList());
    }

    @Override
    public List<ResultGamesDto> findResultOrthographyByUsername(String username) throws ValidationException {
        User user = userRepository.findOneByUsername(username);
        List<ResultOrthography> resultOrthographyList = resultOrthographyRepository.findAllByUser(user);
        List<ResultGamesDto> resultGamesDtoList = new ArrayList<>();
        for (ResultOrthography resultOrthography : resultOrthographyList) {
            ResultGamesDto resultGamesDto = new ResultGamesDto();
            resultGamesDto.setName(resultOrthography.getOrthography().getWord());
            resultGamesDto.setGood(resultOrthography.getGood());
            resultGamesDto.setWrong(resultOrthography.getWrong());
            resultGamesDtoList.add(resultGamesDto);
        }
        return resultGamesDtoList;
    }

    @Override
    public void saveResultGame(ResultGamesDto resultGamesDto) throws ValidationException {
        User user = authenticationService.getCurrentlyLoggedUser();
        if (user == null) {
            throw new ValidationException("Child cannot be null");
        }
        //Integer digit = resultDigitsRepository.findOne(resultDigitDto.getDigit());

        ResultGames resultGames = null;
        NewGame newGame = newGameRepository.findOneByNameGame(resultGamesDto.getNameGame());
        if (newGame == null) {
            throw new ValidationException("Game not exist");
        }
        Optional<ResultGames> resultGamesOptional = resultGamesRepository.findOneByUserAndNewGameAndName(user, newGame, resultGamesDto.getName());
        if (resultGamesDto.getAright()) {
            if (resultGamesOptional.isPresent()) {
                resultGames = resultGamesOptional.get();
                resultGames.increaseGood();
            } else {
                resultGames = new ResultGames();
                resultGames.setNewGame(newGame);
                resultGames.setName(resultGamesDto.getName());
                resultGames.setPoint(resultGamesDto.getPoint());
                resultGames.setGood(1);
                resultGames.setUser(user);
            }
        } else {
            if (resultGamesOptional.isPresent()) {
                resultGames = resultGamesOptional.get();
                resultGames.increaseWrong();
            } else {
                resultGames = new ResultGames();
                resultGames.setNewGame(newGame);
                resultGames.setName(resultGamesDto.getName());
                resultGames.setPoint(resultGamesDto.getPoint());
                resultGames.setWrong(1);
                resultGames.setUser(user);
            }
        }
        resultGamesRepository.save(resultGames);
    }


    @Override
    public List<ResultGamesDto> findResultGamesByUsernameAndNameGame(String username, String nameGame) throws ValidationException {
        User user = userRepository.findOneByUsername(username);
        NewGame newGame = newGameRepository.findOneByNameGame(nameGame);
        List<ResultGames> resultGamesList = resultGamesRepository.findAllByUserAndNewGame(user, newGame);
        List<ResultGamesDto> resultGamesDtoList = new ArrayList<>();
        for (ResultGames resultGames : resultGamesList) {
            ResultGamesDto resultGamesDto = new ResultGamesDto();
            resultGamesDto.setName(resultGames.getName());
            resultGamesDto.setPoint(resultGames.getPoint());
            resultGamesDto.setGood(resultGames.getGood());
            resultGamesDto.setWrong(resultGames.getWrong());
            resultGamesDtoList.add(resultGamesDto);
        }
        return resultGamesDtoList;
    }

    @Override
    public void saveNewNameGame(NewGameDto newGameDto) throws ValidationException {
        if (newGameRepository.findOneByNameGame(newGameDto.getNameGame()) != null) {
            throw new ValidationException("Name game already exist");
        }
        NewGame newGame = new NewGame(newGameDto);
        newGameRepository.save(newGame);
    }


}
