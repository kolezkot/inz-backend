package com.portal.repository;

import com.portal.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WordsRepository extends JpaRepository<Word, Long> {
}
