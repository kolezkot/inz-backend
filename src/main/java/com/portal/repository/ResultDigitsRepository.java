package com.portal.repository;

import com.portal.model.ResultDigits;
import com.portal.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ResultDigitsRepository extends JpaRepository<ResultDigits, Long> {

    List<ResultDigits> findAllByUser(User user);

    Optional<ResultDigits> findOneByUserAndDigit(User user, Integer digit);
}
