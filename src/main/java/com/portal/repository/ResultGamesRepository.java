package com.portal.repository;

import com.portal.model.NewGame;
import com.portal.model.ResultGames;
import com.portal.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ResultGamesRepository extends JpaRepository<ResultGames, Long> {

    List<ResultGames> findAllByUserAndNewGame(User user, NewGame id);

    Optional<ResultGames> findOneByUserAndName(User user, Integer digit);

    Optional<ResultGames>findOneByUserAndNewGameAndName(User user, NewGame id, String name);

}
