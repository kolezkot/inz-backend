package com.portal.repository;

import com.portal.model.ResultWords;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResultEnglishRepository extends JpaRepository<ResultWords, Long> {
}
