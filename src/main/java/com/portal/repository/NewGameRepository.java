package com.portal.repository;

import com.portal.model.NewGame;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewGameRepository extends JpaRepository<NewGame, Long> {

    NewGame findOneByNameGame(String nameGame);
}
