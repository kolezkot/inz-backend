package com.portal.repository;

import com.portal.model.ResultWords;
import com.portal.model.User;
import com.portal.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ResultWordRepository extends JpaRepository<ResultWords, Long> {

    List<ResultWords> findAllByUser(User user);

    Optional<ResultWords> findOneByUserAndWord(User user, Word word);
}
