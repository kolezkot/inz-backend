package com.portal.repository;

import com.portal.model.Orthography;
import com.portal.model.ResultOrthography;
import com.portal.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ResultOrthographyRepository extends JpaRepository<ResultOrthography, Long> {

    List<ResultOrthography> findAllByUser(User user);

    Optional<ResultOrthography> findOneByUserAndOrthography(User user, Orthography orthography);
}
