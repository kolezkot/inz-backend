package com.portal.repository;

import com.portal.model.Orthography;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrthographyRepository extends JpaRepository<Orthography, Long> {
}
