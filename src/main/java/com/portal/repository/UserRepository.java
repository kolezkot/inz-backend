package com.portal.repository;

import com.portal.dto.UserDto;
import com.portal.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByUsername(String username);

    List<User> findAllByParentUser(User parentUser);

}
