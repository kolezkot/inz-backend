package com.portal.config.jwt;

import com.portal.dto.UserDto;
import com.portal.exception.ValidationException;
import com.portal.model.User;
import com.portal.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthenticationService {

    @Autowired
    private UserRepository userRepository;

    public User getCurrentlyLoggedUser(){
        return userRepository.findOneByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public Map<String, Object> authenticate(UserDto userDto) throws ValidationException {
        String token = null;
        User user = userRepository.findOneByUsername(userDto.getUsername());
        Map<String, Object> tokenMap = new HashMap<String, Object>();
        if (user != null && user.getPassword().equals(userDto.getPassword())) {
            token = Jwts.builder().setSubject(userDto.getUsername()).claim("role", user.getRole()).setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS512, "secretkey").compact();
            tokenMap.put("token", token);
            tokenMap.put("user", user);

        } else {
            throw new ValidationException("Failed to find authenticate");
        }
        return tokenMap;

    }
}
