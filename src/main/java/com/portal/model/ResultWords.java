package com.portal.model;

import com.portal.dto.ResultWordDto;
import com.portal.dto.WordDto;

import javax.persistence.*;

@Entity
public class ResultWords {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Word word;

    private Integer good = 0;

    private Integer wrong = 0;

    public ResultWords() {
    }

    public void increaseGood(){
        this.good += 1;
    }

    public void increaseWrong(){
        this.wrong += 1;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getWrong() {
        return wrong;
    }

    public void setWrong(Integer wrong) {
        this.wrong = wrong;
    }
}
