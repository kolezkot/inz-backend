package com.portal.model;

import com.portal.dto.OrthographyDto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Orthography {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String word;

    private String wordAnglish;

    private String wrongLetter;

    private String pathImage;

    private String pathSound;

    public Orthography() {

    }

    public Orthography(OrthographyDto orthographyDto){
        this.id = orthographyDto.getId();
        this.word = orthographyDto.getWord();
        this.wordAnglish = orthographyDto.getWordAnglish();
        this.wrongLetter = orthographyDto.getWrongLetter();
        this.pathImage = orthographyDto.getPathImage();
        this.pathSound = orthographyDto.getPathSound();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWordAnglish() {
        return wordAnglish;
    }

    public void setWordAnglish(String wordAnglish) {
        this.wordAnglish = wordAnglish;
    }

    public String getWrongLetter() {
        return wrongLetter;
    }

    public void setWrongLetter(String wrongLetter) {
        this.wrongLetter = wrongLetter;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public String getPathSound() {
        return pathSound;
    }

    public void setPathSound(String pathSound) {
        this.pathSound = pathSound;
    }
}
