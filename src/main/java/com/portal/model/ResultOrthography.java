package com.portal.model;

import javax.persistence.*;

@Entity
public class ResultOrthography {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Orthography orthography;

    private Integer good = 0;

    private Integer wrong = 0;

    public ResultOrthography() {
    }

    public void increaseGood(){
        this.good += 1;
    }

    public void increaseWrong(){
        this.wrong += 1;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Orthography getOrthography() {
        return orthography;
    }

    public void setOrthography(Orthography orthography) {
        this.orthography = orthography;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getWrong() {
        return wrong;
    }

    public void setWrong(Integer wrong) {
        this.wrong = wrong;
    }
}
