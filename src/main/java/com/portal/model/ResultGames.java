package com.portal.model;

import javax.persistence.*;

@Entity
public class ResultGames {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private NewGame newGame;

    @ManyToOne
    private User user;

    private String name;

    private Integer point;

    private Integer good = 0;

    private Integer wrong = 0;

    public ResultGames() {
    }

    public void increaseGood(){
        this.good += 1;
    }

    public void increaseWrong(){
        this.wrong += 1;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getWrong() {
        return wrong;
    }

    public void setWrong(Integer wrong) {
        this.wrong = wrong;
    }

    public NewGame getNewGame() {
        return newGame;
    }

    public void setNewGame(NewGame newGame) {
        this.newGame = newGame;
    }

}
