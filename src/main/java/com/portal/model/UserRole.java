package com.portal.model;

public enum UserRole {
    PARENT, CHILD, ADMIN
}
