package com.portal.model;

import com.portal.dto.WordDto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String word;

    private String pathWord;

    private String englishWord;

    public Word() {

    }

    public Word(WordDto wordDto) {
        this.word=wordDto.getWord();
        this.pathWord=wordDto.getPathWord();
        this.englishWord =wordDto.getEnglishWord();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPathWord() {
        return pathWord;
    }

    public void setPathWord(String pathWord) {
        this.pathWord = pathWord;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

}
