package com.portal.rest;

import com.portal.config.jwt.AuthenticationService;
import com.portal.dto.UserDto;
import com.portal.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
public class SecurityController {


    private Logger logger = LoggerFactory.getLogger(SecurityController.class);

    @Autowired
    private AuthenticationService authenticationService;

    /**
     * @param userDto used to get username and password
     * @return JSON contains token and user after success authentication.
     */
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> login(@RequestBody UserDto userDto) throws IOException {
        try {
            return ResponseEntity.ok(authenticationService.authenticate(userDto));
        } catch (ValidationException e) {
            logger.error("Validation error", e);
            return ResponseEntity.badRequest().body(null);
        } catch (Exception e) {
            logger.error("Internal server error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }
}
