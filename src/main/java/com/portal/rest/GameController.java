package com.portal.rest;

import com.portal.dto.*;
import com.portal.exception.ValidationException;
import com.portal.model.NewGame;
import com.portal.repository.ResultWordRepository;
import com.portal.service.GameDigitsService;
import com.portal.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class GameController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    private GameService gameService;

    private GameDigitsService gameDigitsService;

    private ResultWordRepository resultWordRepository;


    @Autowired
    public GameController(GameService gameService, GameDigitsService gameDigitsServiceService, ResultWordRepository resultWordRepository) {
        this.gameService = gameService;
        this.gameDigitsService = gameDigitsServiceService;
        this.resultWordRepository = resultWordRepository;
    }

    /**
     * Method for adding result game
     *
     * @param result - result to add
     * @return ResultWords
     */
    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public ResponseEntity<Void> createResultWord(@RequestBody ResultWordDto result) {
        try {
            gameService.saveResultWord(result);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } catch (ValidationException e) {
            logger.error("Child cannot be null", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Method for adding result game
     *
     * @param digitDto - result to add
     * @return ResultWords
     */
    @RequestMapping(value = "/result-digit", method = RequestMethod.POST)
    public ResponseEntity<Void> createResultDigits(@RequestBody DigitDto digitDto) {
        try {
            gameDigitsService.saveResultDigit(digitDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } catch (ValidationException e) {
            logger.error("Child cannot be null ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Method for adding result game
     *
     * @param resultOrthographyDto - resultOrthography to add
     * @return ResultOrthography
     */
    @RequestMapping(value = "/resultOrthography", method = RequestMethod.POST)
    public ResponseEntity<Void> createResultOrthography(@RequestBody ResultOrthographyDto resultOrthographyDto) {
        try {
            gameService.saveResultOrthography(resultOrthographyDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } catch (ValidationException e) {
            logger.error("Child cannot be null", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }


    /**
     * Method to get all words
     *
     * @return list of all Word
     */
    @RequestMapping(value = "/words", method = RequestMethod.GET)
    public ResponseEntity<List<WordDto>> words() {
        return ResponseEntity.ok(gameService.getAllWords());
    }


    /**
     * Method to get all orthography
     *
     * @return list of all Orthography
     */
    @RequestMapping(value = "/orthography", method = RequestMethod.GET)
    public ResponseEntity<List<OrthographyDto>> orthography() {
        return ResponseEntity.ok(gameService.getAllOrthography());
    }

//    @RequestMapping(value = "/wordsy", method = RequestMethod.POST)
//    public ResponseEntity<List<ResultWordDto>> cos(@RequestBody UserDto user) {
//        return ResponseEntity.ok(userService.getAllChildUsers(parentUser));
//    }

    /**
     * Method to fetch user by id
     *
     * @param username - username that is used to find user
     * @return user
     */
    @PreAuthorize("hasRole('PARENT')")
    @RequestMapping(value = "/result-abc/{username}", method = RequestMethod.GET)
    public ResponseEntity<List<ResultGamesDto>> getResultWordsById(@PathVariable("username") String username) {
        try {
            return ResponseEntity.ok(gameService.findResultWordByUsername(username));
        } catch (ValidationException e) {
            logger.error("Username not found", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Method to fetch user by id
     *
     * @param username - username that is used to find user
     * @return user
     */
    @PreAuthorize("hasRole('PARENT')")
    @RequestMapping(value = "/result-digit/{username}", method = RequestMethod.GET)
    public ResponseEntity<List<ResultGamesDto>> getResultDigitById(@PathVariable("username") String username) {
        try {
            return ResponseEntity.ok(gameService.findResultDigitByUsername(username));
        } catch (ValidationException e) {
            logger.error("Username not found", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Method to fetch user by id
     *
     * @param username - username that is used to find user
     * @return user
     */
    @PreAuthorize("hasRole('PARENT')")
    @RequestMapping(value = "/result-orthography/{username}", method = RequestMethod.GET)
    public ResponseEntity<List<ResultGamesDto>> getResultOrthographyById(@PathVariable("username") String username) {
        try {
            return ResponseEntity.ok(gameService.findResultOrthographyByUsername(username));
        } catch (ValidationException e) {
            logger.error("Username not found", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Method for adding result game
     *
     * @param resultGamesDto - resultGame to add
     * @return ResultGames
     */
    @RequestMapping(value = "/save-game", method = RequestMethod.POST)
    public ResponseEntity<Void> createResultGames(@RequestBody ResultGamesDto resultGamesDto) {
        try {
            gameService.saveResultGame(resultGamesDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } catch (ValidationException e) {
            logger.error("Username already exist game ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Method to fetch user by id
     *
     * @param username - username that is used to find user
     * @return user
     */
    @RequestMapping(value = "/result-game/{username}/{nameGame}", method = RequestMethod.GET)
    public ResponseEntity<List<ResultGamesDto>> getResultGamesByIdAndNameGame(@PathVariable("username") String username, @PathVariable("nameGame") String nameGame) {
        try {
            return ResponseEntity.ok(gameService.findResultGamesByUsernameAndNameGame(username, nameGame));
        } catch (ValidationException e) {
            logger.error("Username not found", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    /**
     * Method for adding a newGame
     *
     * @param newGameDto - newGame to add
     * @return newGame
     */
    @RequestMapping(value = "/game-name", method = RequestMethod.POST)
    public ResponseEntity<Void> createNewGame(@RequestBody NewGameDto newGameDto) {
        try {
            gameService.saveNewNameGame(newGameDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } catch (ValidationException e) {
            logger.error("Name game already exist ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

}
