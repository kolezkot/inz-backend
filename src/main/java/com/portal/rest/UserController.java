package com.portal.rest;

import com.portal.dto.ResultWordDto;
import com.portal.dto.UserDto;
import com.portal.exception.ValidationException;
import com.portal.model.User;
import com.portal.model.UserRole;
import com.portal.repository.UserRepository;
import com.portal.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    /**
     * Method to get all users
     *
     * @return list of all User
     */
    @PreAuthorize("hasRole('PARENT')")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<UserDto>> users() {
        return ResponseEntity.ok(userService.getAllUsers());
    }


    /**
     * Method to fetch user by id
     *
     * @param username - username that is used to find user
     * @return user
     */
    @PreAuthorize("hasRole('PARENT')")
    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET)
    public ResponseEntity<UserDto> getUserById(@PathVariable("username") String username) {
        try {
            return ResponseEntity.ok(userService.findUserById(username));
        } catch (ValidationException e) {
            logger.error("Username not found", e);
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PreAuthorize("hasRole('PARENT')")
    @RequestMapping(value = "/children", method = RequestMethod.POST)
    public ResponseEntity<List<UserDto>> usersChild(@RequestBody UserDto parentUser) {
        return ResponseEntity.ok(userService.getAllChildUsers(parentUser));
    }

    /**
     * Method for adding a user
     *
     * @param user - user to add
     * @return User
     */
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody UserDto user) {
        try {
            userService.register(user);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } catch (ValidationException e) {
            logger.error("Username already exist ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }


    /**
     * Method for adding a user
     *
     * @param user - user to add
     * @return User
     */
    @PreAuthorize("hasRole('PARENT')")
    @RequestMapping(value = "/user-child", method = RequestMethod.POST)
    public ResponseEntity<Void> createChildUser(@RequestBody UserDto user) {
        try {
            userService.registerChild(user);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } catch (ValidationException e) {
            logger.error("Username already exist ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }


    /**
     * Method for editing an user details
     *
     * @param userDto - user to edit
     * @return modified user
     */
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateUser(@RequestBody UserDto userDto) {
        try {
            userService.updateUser(userDto);
            return ResponseEntity.ok().body(null);
        } catch (ValidationException e) {
            logger.error("Username doesn't exist ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @RequestMapping(value = "/mock-admin", method = RequestMethod.GET)
    public ResponseEntity<Void> mockAdmin(){
        User user = new User();
        user.setRole(UserRole.ADMIN);
        user.setUsername("admin");
        user.setPassword("admin");
        userRepository.save(user);
        return ResponseEntity.ok().body(null);
    }

}